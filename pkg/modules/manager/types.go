package manager

import (
	"errors"
	"github.com/natefinch/pie"
	"log"
	"os"
	"strings"
)

type Module struct {
	config ModuleConfig
	path   string
	server *pie.Server
	loaded bool
}

// to make some attriutes uneditable but accessable by externals
func (m *Module) GetConfig() ModuleConfig {
	return m.config
}

func (m *Module) GetPath() string {
	return m.path
}


func (m *Module) GetLoaded() bool {
	return m.loaded
}

type ModuleConfig struct {
	URI string // eg gitlab.com/someone/some_module
	Name string // eg some module
	Author string // eg someone
	VersionStr string // eg 1.2.3-stable
	VersionHash string // eg 6899ecf77b34d6d3cdbd7f28929bd950403799b4f5ab102feb955a232b5522bb5f2efe5fd954771bc9ac7e56b028bc4ef06a0b35eafedd3fe924f53574bdd65c (hash of "1.2.3-stable" in sha512)
	Methods []string // eg []string{"gitlab.com/someone/another_module/some.method"}
}

func (m *Module) Load(rootNames interface{}) error {
	server, err := pie.StartConsumer(os.Stderr, m.path)
	*m.server = &server
	if err != nil {
		return err
	}
	err = m.registerNames(rootNames interface{}))
	return err
}

func (m *Module) registerNames(rootNames interface{}) []error {
	var errs = make([]error, 0)
	for _, method := range m.config.Methods {
		allName := method[strings.LastIndex(method, ",")+1:]
		//endName := allName[strings.LastIndex(allName, ",")+1:]
		baseName := allName[:strings.LastIndex(allName, ".")+1]
		URI := method[:strings.LastIndex(method, ",")+1]
		if URI == "/" {
			if err := m.server.RegisterName(baseName, rootNames); err != nil {
				errs = append(errs, err)
				continue
			}
		} else {
			errs = append(errs, errors.New("non root URIs not supported yet"))
			continue
		}
		errs = append(errs, nil)
	}
	return errs
}
